"""
@author: Szymon Bocian
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def plot_errorbar(data, mean_name, err_name, err_type, iter_col=[], x_tick_name={}, plot_name="", title=""):
    #err_type -> "coef" gdy w tabeli opisowej jest przedzial (dla bootstrapa), a "num" gdy w tabeli jest liczba (dla standard error)

    fig, ax = plt.subplots(3, 1, figsize = [20, 10])
    pom = 0

    for letter in iter_col:
        zestaw = data[data["Name"].str.startswith(letter)]

        if err_type is "num":
            ax[pom].errorbar(np.arange(np.shape(zestaw)[0]), zestaw[mean_name], yerr=zestaw[err_name], fmt=".", c="#2f5d62", capsize=5, markersize=8)
        elif err_type is "coef":
            err = pd.eval(zestaw[err_name].apply(lambda x: x[1:-1].split(","))).astype(float)
            for i in range(np.shape(err)[0]):
                ax[pom].plot([i, i], err[i, :], "_-", c="#2f5d62", markersize=8)
        else:
            print("Error: zly typ errorbarow - wybierz 'num' lub 'coef'")
        ax[pom].plot(np.arange(np.shape(zestaw)[0]), zestaw[mean_name], ".", c="#2f5d62", markersize=8)
        ax[pom].set_xticks(np.arange(np.shape(zestaw)[0]))
        ax[pom].set_xticklabels(x_tick_name[letter], fontsize = 14) if x_tick_name else ax[pom].set_xticklabels(zestaw["Name"], fontsize = 14)
        ax[pom].text(1.015, 0.5, letter, fontsize=18, transform=ax[pom].transAxes) if x_tick_name else None
        ax[pom].grid()
        pom += 1

    ax[0].set_title(title, fontsize = 22)
    ax[1].set_ylabel("Skala Likerta", fontsize = 18)
    ax[2].set_xlabel("Kod", fontsize = 18)

    plt.savefig(plot_name) if plot_name else plt.show()

if __name__ == "__main__":
    data_story = pd.read_csv(r".\Dane\statystyki_opisowe_historyjki.csv")[["Name", "Mean", "StdErr", "Bootstrap przedzialy"]]
    plot_errorbar(data_story, "Mean", "StdErr", "num", ["A", "B", "C"], {}, ".\Wykresy\Errorbars_historyjki_stderr.png",
                  "Wykres średnich i odchyleń standardowych średniej dla historyjek")
    plot_errorbar(data_story, "Mean", "Bootstrap przedzialy", "coef", ["A", "B", "C"], {}, ".\Wykresy\Errorbars_historyjki_bootstrap.png",
                  "Wykres średnich i 95% przedziału ufności bootstrap dla historyjek")

    data_sentence = pd.read_csv(r".\Dane\statystyki_opisowe_zdania.csv")[["Name", "Mean", "StdErr", "Bootstrap przedzialy"]]
    sentence_xtick = {}
    for kit in ["Z1", "Z2", "Z3"]:
        data_kit = data_sentence[data_sentence["Name"].str.startswith(kit)]
        sentence_xtick[kit] = [i.split("[")[1][:-1] for i in data_kit["Name"]]
    plot_errorbar(data_sentence, "Mean", "StdErr", "num", ["Z1", "Z2", "Z3"], sentence_xtick, "Wykresy\Errorbars_zdania_stderr.png",
                  "Wykres średnich i odchyleń standardowych dla zdań")
    plot_errorbar(data_sentence, "Mean", "Bootstrap przedzialy", "coef", ["Z1", "Z2", "Z3"], sentence_xtick, "Wykresy\Errorbars_zdania_bootstrap.png",
                  "Wykres średnich i 95% przedziału ufności bootstrap dla zdań")


