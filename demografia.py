"""
@author: Szymon Bocian
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def demografia(data_input):

    #Potrzebne informacje
    n_gender = {"F": 0, "M": 1}
    n_education = {"A1": "podstawowe", "A2": "gimnazjalne", "A3": "zasadnicze\nzawodowe", "A4": "średnie", "A5": "wyższe"}
    n_phil = {"N": "Niefilozof", "Y": "Filozof"}
    n_eug = {"A1": "Tak", "A2": "Kojarzę", "A3": "Nie"}

    gender = data_input["p01"].replace(n_gender) #płeć
    birth_year = data_input["p02"][data_input["p02"] > 1919] #rok narodzin
    education = data_input["p03"].replace(n_education) #edukacja
    education = dict(education.value_counts())
    phil_edu = data_input["p04"].replace(n_phil) #edukacja filozoficzna
    phil_edu = dict(phil_edu.value_counts())
    eugenes = data_input["SprE[SQ001]"].replace(n_eug) #znajomość eugeniki
    eugenes = dict(eugenes.value_counts())

    #Zapisanie informacji w pliku txt
    file = open(".\Dane\demografia.txt", "w")

    file.write("Ilość badanych: " + str(len(gender)))
    file.write("\n\n")
    file.write("Płeć (procent || liczba): \n" + "   mężczyźni: " + str(round(100*np.mean(gender), 2)) + " || " + str(np.sum(gender)) + "\n   kobiety: " + str(round(100*(1 - np.mean(gender)), 2)) + " || " + str(len(gender) - np.sum(gender)))
    file.write("\n\n")
    file.write("Średni wiek badanych +- odchyl. stand. średniej: " + str(round((2019 - np.mean(birth_year)), 2)) + " +- " + str(round(np.std((2019 - birth_year), ddof = 1)/np.sqrt(len(gender)), 2)))
    file.write("\n\n")
    file.write("Mediana wieku badanych: " + str((2019 - np.median(birth_year))))
    file.write("\n\n")
    file.write("Przedział wiekowy: " + str((2019 - np.max(birth_year))) + " - " + str((2019 - np.min(birth_year)))) #+ "(" + str((2019 - np.sort(birth_year)[1])) + ")")
    file.write("\n\n")
    file.write("Wykształcenie (liczba || procent): \n")
    for wyk in education:
        file.write("    " + str(wyk) + ": " + str(education[wyk]) + " || " + str(round(education[wyk]/len(gender) * 100, 2)) + "\n")
    file.write("\n")
    file.write("Wykształcenie filozoficzne (liczba || procent): \n")
    for wyk in phil_edu:
        file.write("    " + str(wyk) + ": " + str(phil_edu[wyk]) + " || " + str(round(phil_edu[wyk]/len(gender) * 100, 2)) + "\n")
    file.write("\n")
    file.write("Czy ludzie wiedzą co to eugenika (liczba || procent): \n")
    for wyk in eugenes:
        file.write("    " + str(wyk) + ": " + str(eugenes[wyk]) + " || " + str(round(eugenes[wyk]/len(gender) * 100, 2)) + "\n")

    file.close()

    #Tworzenie wykresu
    fig = plt.figure(figsize=[20, 10])
    ax1 = plt.subplot2grid((5, 4), (0, 0), colspan=4, rowspan = 3) #histogram wieku
    ax2 = plt.subplot2grid((5, 4), (3, 0), colspan=2, rowspan = 2) #barplot wykształcenia
    ax3 = plt.subplot2grid((5, 4), (3, 2), colspan=2, rowspan = 2) #barplot znajomosci eugeniki

    n, bins, _ = ax1.hist(np.array(2019 - birth_year), bins = 60, edgecolor = "black", facecolor = "green", alpha = 0.35)
    ax1.set_xticks(bins[::5])
    ax1.set_yticks(np.arange(0, np.max(n)+10, 5))
    ax1.set_title("Wiek", fontsize = 22)
    ax1.set_xlabel("Wiek [lata]", fontsize = 18)
    ax1.set_ylabel("Liczebność", fontsize = 18)
    ax1.tick_params(labelsize=14)
    ax1.text(-0.047, 1, "a)", fontsize = 28, transform=ax1.transAxes)

    ax1.plot([2019 - np.mean(birth_year), 2019 - np.mean(birth_year)], [0, np.max(np.arange(0, np.max(n)+10, 5))], "orange", linewidth = 2, label = r"$\bar x$ = {}".format(round(2019 - np.mean(birth_year), 2)))
    ax1.plot([2019 - np.median(birth_year), 2019 - np.median(birth_year)], [0, np.max(np.arange(0, np.max(n)+10, 5))], "blue", linewidth = 2, label = f"Me(x) = {2019 - np.median(birth_year)}")
    ax1.legend(fontsize = "xx-large", loc = "upper left")

    ax2.bar(list(education.keys())[::-1], list(education.values())[::-1], 0.35, edgecolor = "black", facecolor = "green", alpha = 0.35)
    ax2.set_title("Wykształcenie", fontsize = 22)
    ax2.set_ylabel("Liczebność", fontsize = 18)
    ax2.set_yticks(np.arange(0, 301, 50))
    ax2.tick_params(labelsize=14)
    ax2.text(-0.1, 1, "b)", fontsize = 28, transform=ax2.transAxes)

    for rect, hgh in zip(ax2.patches, list(education.values())[::-1]):
        ax2.text(rect.get_x() + rect.get_width()/2, hgh+3, str(hgh), ha = 'center', va = "bottom", fontsize = 15)

    ax3.bar(list(eugenes.keys())[::-1], list(eugenes.values())[::-1], 0.35, edgecolor = "black", facecolor = "green", alpha = 0.35)
    ax3.set_title("Znajomość eugeniki", fontsize = 22)
    ax3.set_ylabel("Liczebność", fontsize = 18)
    ax3.set_yticks(np.arange(0, 460, 150))
    ax3.tick_params(labelsize=14)
    ax3.text(-0.1, 1, "c)", fontsize = 28, transform=ax3.transAxes)

    for rect, hgh in zip(ax3.patches, list(eugenes.values())[::-1]):
        ax3.text(rect.get_x() + rect.get_width()/2, hgh+3, str(hgh), ha = 'center', va = "bottom", fontsize = 15)

    fig.tight_layout(pad=1.8)#, w_pad=0.5, h_pad=1.0)

    fig.savefig(".\Wykresy\demografia_hist.png")

if __name__ == "__main__":
    data_input = pd.read_csv(r".\Dane\dane.csv")
    demografia(data_input)