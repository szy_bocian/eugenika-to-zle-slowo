"""
@author: Szymon Bocian
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def violinplots(data, iter_col=[], x_tick_name={}, plot_name="", title=""):

    fig, ax = plt.subplots(3, 1, figsize = [20, 10])
    pom = 0

    for letter in iter_col:
        zestaw = data[data["Name"].str.startswith(letter)]
        ankiety = [list(map(int, row.split(","))) for row in zestaw["Wyniki ankiet"]]

        parts_viol = ax[pom].violinplot(ankiety, showmedians=False, showmeans=False, showextrema=False)
        for pc in parts_viol['bodies']:
            pc.set_facecolor('#C0EC83')
            pc.set_edgecolor('#547A1D')
            #pc.set_alpha(0.3)

        parts_box = ax[pom].boxplot(ankiety, widths=[0.05]*len(zestaw), patch_artist=True)
        for pc in parts_box["boxes"]:
            pc.set_facecolor("black")
            pc.set_linewidth(0)
            pc.set_alpha(0.35)
        for pc in parts_box["caps"]:
            pc.set_linewidth(0)
        for pc in parts_box["medians"]:
            ax[pom].plot(pc.get_xdata(), pc.get_ydata(), color="black", lw=1.5)
            # ax[pom].plot(pc.get_xdata()[0]+0.025, pc.get_ydata()[0], marker="x", color="black", lw=1.5, ms=10)
            pc.set_linewidth(0)
            # pc.set_color("black")
        for pc in parts_box["whiskers"]:
            pc.set_color("black")
            pc.set_alpha(0.35)
        for pc in parts_box["fliers"]:
            pc.set_markersize(2.75)
            pc.set_marker(".")

        ax[pom].set_xticks(np.arange(1, np.shape(zestaw)[0]+1))
        ax[pom].set_xticklabels(x_tick_name[letter], fontsize = 14) if x_tick_name else ax[pom].set_xticklabels(zestaw["Name"], fontsize = 14)
        ax[pom].yaxis.set_tick_params(labelsize=14)
        ax[pom].text(1.015, 0.5, letter, fontsize=18, transform=ax[pom].transAxes) if x_tick_name else None
        pom += 1

    ax[0].set_title(title, fontsize = 22)
    ax[1].set_ylabel("Skala Likerta", fontsize = 18)
    ax[2].set_xlabel("Kod", fontsize = 18)

    plt.savefig(plot_name) if plot_name else plt.show()

if __name__ == "__main__":
    data_story = pd.read_csv(r".\Dane\statystyki_opisowe_historyjki.csv")[["Name", "Wyniki ankiet"]]
    violinplots(data_story, ["A", "B", "C"], {}, "Wykresy\Violinplots_historyjki.png", title="Violinploty i boxploty dla pytań do historyjek")
    data_sentence = pd.read_csv(r".\Dane\statystyki_opisowe_zdania.csv")[["Name", "Wyniki ankiet"]]
    sentence_xtick = {}
    for kit in ["Z1", "Z2", "Z3"]:
        data_kit = data_sentence[data_sentence["Name"].str.startswith(kit)]
        sentence_xtick[kit] = [i.split("[")[1][:-1] for i in data_kit["Name"]]
    violinplots(data_sentence, ["Z1", "Z2", "Z3"], sentence_xtick, "Wykresy\Violinplots_zdania.png", title="Violinploty i boxploty dla zdań")
