"""
@author: Szymon Bocian
"""

import pandas as pd
import numpy as np
import scipy.stats as st

def maping_data(data_input, names): #mapowanie wartosci z "Ax" na liczby
    
    likert = {"A1": -2, "A2": -1, "A3": 0, "A4": 1, "A5": 2}
    data_output = {} #zmapowane wartosc
    
    for i in names:
        helper = data_input[i].dropna()
        if "[SQ001]" in i:
            data_output[i[:-7]] = np.array(helper.replace(likert))
        else:
            data_output[i] = np.array(helper.replace(likert))

    return data_output
   
def randsample(x, N): #funkcja dla bootstrapa losujaca N elementów z wektora x
	n=len(x)
	ind = np.random.randint(n, size = N)
	y = x[ind]
	return y

def bootstrap_interval(data_input, length, N_rep = 50000): #95% przedzialu ufnosci od bootstrapu
    mean_boot = np.zeros(N_rep)
    for i in range(N_rep):
        pom = randsample(np.array(data_input), length)
        mean_boot[i] = np.mean(pom)
    return (round(st.scoreatpercentile(mean_boot, per = 0.025*100), 4), round(st.scoreatpercentile(mean_boot, per = (1-0.025)*100), 4))

def stat_machine(data_input, names): #zbieranie informacji statystycznych
    
    data_output = {}
    
    nicer_name = [i[:-7] if "[SQ001]" in i else i for i in names] #names jest z [SQ001] dlatego to usuwamy

    data_output["Name"] = nicer_name #nazwa
    data_output["Len"] = [] #wielkosc probki
    data_output["Mean"] = [] #srednia probki
    data_output["Std"] = [] #odchyl. standardowe nieobciązone (ddof = n-1)
    data_output["StdErr"] = [] #odcyl. stand. sredniej (standard error)
    data_output["Median"] = [] #mediana
    data_output["t.Shapiro-Wilk"] = [] #test shapiro wilka na normalnosc
    data_output["t.Koomogorow-Smirnow"] = [] #test kologomorowa smirnowa na normalnosc
    data_output["Bootstrap przedzialy"] = [] #przedzialy bootstrapa
    data_output["StdErr przedzialy (1.96sigma)"] = [] #przedziały 2 odchylen standardowej sredniej
    data_output["Wyniki ankiet"] = [] #wyniki ankiet
      
    for i in nicer_name:
        data_output["Len"].append(len(data_input[i])) 
        data_output["Mean"].append(round(np.mean(data_input[i]), 4)) 
        data_output["Std"].append(round(np.std(data_input[i], ddof = 1), 4))
        data_output["StdErr"].append(round(((np.std(data_input[i], ddof = 1))/np.sqrt(len(data_input[i]))), 4))
        data_output["Median"].append(np.median(data_input[i])) 
        data_output["t.Shapiro-Wilk"].append(np.format_float_scientific(st.shapiro(data_input[i])[1], precision=4))
        data_output["t.Koomogorow-Smirnow"].append(np.format_float_scientific(st.kstest(data_input[i], 'norm', args=(np.mean(data_input[i]), np.std(data_input[i], ddof = 1)))[1], precision=4))
        data_output["Bootstrap przedzialy"].append(bootstrap_interval(data_input[i], len(data_input[i])))
        data_output["StdErr przedzialy (1.96sigma)"].append((round(np.mean(data_input[i])-1.96*(np.std(data_input[i], ddof = 1))/np.sqrt(len(data_input[i])), 4), round((np.mean(data_input[i])+1.96*(np.std(data_input[i], ddof = 1))/np.sqrt(len(data_input[i]))), 4)))
        data_output["Wyniki ankiet"].append(str(list(data_input[i]))[1:-1])
        
    return pd.DataFrame(data_output)

def prepare_table_for_anova(data_input):
    data_output = pd.DataFrame(dict(Nazwa=[], Wartosc=[], Rodzaj=[], Eugenika=[], Podmiot=[], Dzialanie=[], Powod=[], Doradca=[], Zawod=[]))
    persons = {"A1": "Robinsonowie", "A2": "Robinsonowie", "B1": ["Lekarz", "Susan"], "B2": ["Urzednik", "Susan"], "B3": "Susan", "C1": ["Rzad", "Alice"], "C2": ["Rzad", "Alice"]}
    actions = {"A1": "Inz.genet.", "A2": "Inz.genet", "B1": "Aborcja", "B2": "Aborcja", "B3": "Aborcja", "C1": "Sterylizacja", "C2": "Sterylizacja"}
    reasons = {"A1": "Naprawa", "A2": "Wyglad", "B1": "Eliminacja", "B2": "Eliminacja", "B3": "Eliminacja", "C1": "Bieda", "C2": "Wada"}

    for question in data_input:
        num = len(data_input[question])
        typ = "Dzialanie" if int(question[-1])%2 else "Podmiot"
        eug = str("E" in question)

        if isinstance(persons[question[:2]], list):
            per = persons[question[:2]][1] if "3" in question or "4" in question else persons[question[:2]][0]
        else:
            per = persons[question[:2]]

        act = actions[question[:2]]
        rea = reasons[question[:2]]

        if question.startswith(("B1", "B2")) and per == "Susan":
            dor = "Ktos"
        elif question.startswith("B3") and per == "Susan":
            dor = "Sobie"
        else:
            dor = None

        job = per if per in ["Lekarz", "Urzednik"] else None
        data_output = data_output.append(pd.DataFrame(dict(Nazwa=[question]*num, Wartosc=data_input[question], Rodzaj=[typ]*num,
                                                       Eugenika=[eug]*num, Podmiot=[per]*num, Dzialanie=[act]*num,
                                                       Powod=[rea]*num, Doradca=[dor]*num, Zawod=[job]*num)), ignore_index=True)

    return data_output

if __name__ == "__main__":

    all_data = pd.read_csv(r".\Dane\dane.csv") #cały plik z danymi

    #HISTORYJKI~~~~~~~~~~~~~~~~~~~~~~~~
    story_name = [i for i in all_data.columns if "[SQ001]" in i] #nagłowki pytań w historyjkach
    story_name.remove('SprE[SQ001]')

    response_story = pd.DataFrame(all_data, columns = story_name) #odpowiedzi ankietowanych w historyjkach
    response_story_maped = maping_data(response_story, story_name) #zmapowane odpowiedzi ankietyowanych w historyjkach

    #stat_analysis_story = stat_machine(response_story_maped, story_name) #spisanie najważniejszych statystyk opisowych i odpowiedzi z ankiet w historyjkach

    data_for_anova_story = prepare_table_for_anova(response_story_maped) #spisanie odpowiedzi i uporzadkowanie ich w historyjkach

    #stat_analysis_story.to_csv(".\Dane\statystyki_opisowe_historyjki.csv")
    data_for_anova_story.to_csv(".\Dane\zbior_odp_historyjki.csv")

    #ZDANIA~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # sentence_name = [i for i in all_data.columns if any(j in i for j in ["Z1[", "Z2[", "Z3["])] #nagłowki pytań w zdaniach
    #
    # response_sentence = pd.DataFrame(all_data, columns=sentence_name) #odpowiedzi ankietowanych w zdaniach
    # response_sentence_maped = maping_data(response_sentence, sentence_name)
    #
    # stat_analysis_sentence = stat_machine(response_sentence_maped, sentence_name) #spisanie najważniejszych statystyk opisowych i odpowiedzi z ankiet w zdaniach
    # stat_analysis_sentence.to_csv(r".\Dane\statystyki_opisowe_zdania.csv")