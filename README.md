# Eugenika to złe slowo

## 1. Instalacja

<przyszłość>

## 2. Ogólne informacje

Celem programu było przeprowadzenie odpowiednich analiz do licencjatu z filozofii eksperymentalnej. Program skłąda się z
dwóch części: grupy plików związanych z analizą korpusową i grupy plików związanych z analizą kwestionariuszową.

Celem badania było udowodnić, że "eugenika" jest słowem nacechowanym negatywnie oraz sprawdzić czy słowo to wpływało na
oceny moralne podmiotów i ich działań, gdy wykonywali czynności uznane za eugeniczne i opis tych czynności zawierał te
słowo.

W **głównym folderze** znajdują się pliki napisane w Python 3.x odpowiedzialne za analizę statystyczną (testy
statystyczne i statystyki deskryptywne) i tworzenie wykresów.

W folderze **Dane** są zebrane wszystkie macierze danych na których przeprowadzano analizy, macierze które zostały
stworzone po porządkowaniu surowych danych oraz pliki zbierające wyniki z testów statystycznych. Formaty pików w tym
folderze to *.csv i *.txt.

W folderze **Wykresy** znajdują się wszystkie wykresy stworzone podczas analizy. Głównie to pliki o formacie .png.

## 3. Opis analiz

### 3.1 Analiza korpusowa

W analizie korpusowej wykorzystano informacje zebrane z Narodowego Korpusu Języka Polski (NKJP) za pomocą wyszukiwarki
korpusowej [PELCRA](http://www.nkjp.uni.lodz.pl/index.jsp). Wykorzystanym korpusem był korpus zrównoważony. Korpus
zrównoważony ma mieć podobne proporcje do wcześniej ustalonego rozkładu tekstów z całego korpusu. Dodatkowo twórcy NKJP
postawili sobie za cel, aby korpus był reprezentatywny, tj. aby jego budowaodpowiadała poczuciu językowemu zwykłych
ludzi.

W korpusie były dwa rodzaje form przekazu:

* *typ*  -->  kategoryzowanie tekstów za pomocą typów odbywa się poprzez ich podział na dość tradycyjnie rozumiane
  rodzaje i gatunki stylistyczne
* *kanał*  -->  związany jest z tym, w jaki sposób tekst jest przekazywany odbiorcy

#### 3.1.1 Spis programów

*Spis programów*:

* analiza_korpusowa.py

*Spis macierzy danych*:

* nkjp_kanal.csv
* nkjp_typ.csv

*Spis analiz*:

* korpus_chi2.txt

*Spis wykresów*:

* analiza_kanal.png
* analiza_typ.png

#### 3.1.2 Opis programów

<opis działania poszczególnych programów>

### 3.2 Analiza kwestionariuszowa

Dane zebrane w tej części analizy zostały za pomocą badań kwestioariuszowych. Głównym celem było sprawdzenie jak
opisanie słowem "eugenika" działań uznanych za eugeniczne wpływa na oceny moralne tych działań i podmiotów je
wykonujących.

#### 3.2.2 Spis programów

*Spis programów*:

* analiza_anova.py
* demografia.py
* preanaliza.py
* wykres_srednich.py
* wykres_violinplot.py

*Spis macierzy danych*:

* dane.csv
* statystyki_opisowe_historyjki.csv
* statystyki_opisowe_zdania.csv
* zbior_odp_historyjki.csv

*Spis analiz*:

* ANOVA_tabelka_historyjki.txt
* demografia.txt

*Spis wykresów*:

* demografia_hist.png
* errorbar_ANOVA_std_hist_A.png
* errorbar_ANOVA_std_hist_B_doradca.png
* errorbar_ANOVA_std_hist_B_podmiot.png
* errorbar_ANOVA_std_hist_B_zawod.png
* errorbar_ANOVA_std_hist_C.png
* Errorbars_historyjki_bootstrap.png
* Errorbars_historyjki_stderr.png
* Errorbars_zdania_bootstrap.png
* Errorbars_zdania_stderr.png
* Violinplots_historyjki.png
* Violinplots_zdania.png

#### 3.2.2 Opis programów

<opis działania poszczególnych programów>

## 4. Korzystanie z programów

<przyszłość>

## 5. Twórcy

Program powstał w ramach projektu realizowanego
w [Laboratorium Filozofii Eksperymentalnej "Kognilab"](http://kognilab.pl/wordpress/)
na Wydziale Filozofii UW.

* **Szymon Bocian** (@szy_bocian) - twórca programu
* **Katarzyna Kuś** - pomoc merytoryczna
