"""
@author: Szymon Bocian
"""
import numpy as np
import pandas as pd
import scipy.stats as st

def analiza_korpusowa(data, txt_title, x_bar, x_label, plot_title, name_file):

    eugenika_sum = data[["eugenika"]].sum()
    eugeniczny_sum = data[["eugeniczny"]].sum()
    korpus_sum = data[["słowa w kategorii"]].sum()

    data[["eugenika"]] = data[["eugenika"]] / eugenika_sum * 100
    data[["słowa w kategorii"]] = data[["słowa w kategorii"]] / korpus_sum * 100
    data[["eugeniczny"]] = data[["eugeniczny"]] / eugeniczny_sum * 100

    #wpisywanie do pliku txt p i chi2
    file = open(".\Dane\korpus_chi2.txt", "a")

    chi2 = st.chisquare(f_obs = data[["eugenika"]], f_exp = data[["słowa w kategorii"]])
    file.write(txt_title + "\n\nEugenika: \n    chi2: " + str(chi2[0][0]) + "\n    p: " + str(chi2[1][0]) + "\n\n")

    chi2 = st.chisquare(f_obs = data[["eugeniczny"]], f_exp = data[["słowa w kategorii"]])
    file.write("Eugeniczny: \n    chi2: " + str(chi2[0][0]) + "\n    p: " + str(chi2[1][0]) + "\n\n")

    data[["eugenika"]] = np.round(data[["eugenika"]], 2)
    data[["eugeniczny"]] = np.round(data[["eugeniczny"]], 2)
    data[["słowa w kategorii"]] = np.round(data[["słowa w kategorii"]], 2)

    #Tworzenie barplotów
    data = data.rename(columns={"słowa w kategorii": "rozkład rzeczywisty"})

    pallete = {"eugenika": "#C0EC83", "eugeniczny": "#96CC39", "rozkład rzeczywisty": "#547A1D"}

    ax = data.plot.bar(x=x_bar, rot = 0, figsize = [20, 10], color = pallete)
    ax.set_ylabel("Procent słów w danej kategorii [%]", fontsize = 18)
    ax.set_xlabel(x_label, fontsize = 18)
    ax.xaxis.set_tick_params(labelsize = 14)
    ax.set_title(plot_title, fontsize = 22)
    ax.legend(fontsize = "xx-large")
    #ax.text(-0.13, 1, "a)", fontsize = 28, transform=ax.transAxes)

    fig = ax.get_figure()
    fig.savefig(name_file) if name_file else fig.show()

if __name__ == "__main__":
    data_typ = pd.read_csv(r".\Dane\nkjp_typ.csv")
    analiza_korpusowa(data_typ, "~~~~TYP~~~~", "Typ", "Typ", "Dane procentowe dla typu", r".\Wykresy\analiza_typ.png")
    data_kanal = pd.read_csv(r".\Dane\nkjp_kanal.csv")
    analiza_korpusowa(data_kanal, "~~~~KANAŁ~~~~", "Kanał", "Kanał", "Dane procentowe dla kanału", r".\Wykresy\analiza_kanal.png")