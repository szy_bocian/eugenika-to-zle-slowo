"""
@author: Szymon Bocian
"""

import pandas as pd
from statsmodels.formula.api import ols
from statsmodels.stats.anova import anova_lm
import seaborn as sns
import numpy as np
from scipy.stats import ttest_ind

def ANOVA_table(input, name_file = ".\Dane\wynik_anova.txt", stats = ["count", "mean", "std", "sem", "median"]):

    file = open(name_file, "w")

    for name, formula, cols, data in input:
        model = ols(formula, data = data).fit()
        anova = anova_lm(model)

        file.write(name + " -> " + formula + "\n\n" + "ANOVA TABLE:\n" + anova.to_string() + "\n\n"
                   + "DESCRIPTION STATS:\n" + data.groupby(cols[1:])[cols[0]].aggregate(stats).to_string() + "\n\n\n")
        file.write("---~~~---"*15)
        file.write("\n")

    file.close()

def ANOVA_plot(data, x, y, hue=None, row=None, col=None, ci="sd", title="", xlab="", ylab="", palette=["#C0EC83", "#547A1D"],
               fontsize=15, top_adj=0.85, yticks=np.arange(-2, 2.5, 0.5), name_file=".\Dane\wykres_anova.png"):

    sns.set_theme(style="ticks", palette=sns.color_palette(palette), rc={'figure.figsize': (20, 10), 'axes.grid': True})

    his = sns.catplot(x=x, y=y,
                hue=hue, row=row, col=col,
                data=data, kind="point",
                dodge=True, join = False, ci = ci, capsize=0.1)
    his.set_ylabels(ylab, fontsize = fontsize)
    his.set_xlabels(xlab, fontsize = fontsize)
    his.fig.subplots_adjust(top=top_adj)
    his.fig.suptitle(title, fontsize=fontsize+4)
    his.despine(top=False, right=False, left=False, bottom=False)
    his.set(yticks=yticks)
    his.savefig(name_file)

if __name__ == "__main__":

    data = pd.read_csv(".\Dane\zbior_odp_historyjki.csv")

    prepared_data_historyjki = [
        ("A hip.1+2", "Wartosc ~ C(Rodzaj) * C(Powod)", ["Wartosc", "Rodzaj", "Powod"], data[data["Nazwa"].str.startswith("A")]),
        ("B hip.1+3+4", "Wartosc ~ C(Rodzaj) * C(Eugenika) * C(Doradca)", ["Wartosc", "Rodzaj", "Eugenika", "Doradca"], data[data["Nazwa"].str.startswith("B")]),
        ("B hip.1+3+5", "Wartosc ~ C(Rodzaj) * C(Eugenika) * C(Zawod)", ["Wartosc", "Rodzaj", "Eugenika", "Zawod"], data[data["Nazwa"].str.startswith("B")]),
        ("B hip.1+3+6", "Wartosc ~ C(Rodzaj) * C(Eugenika) * C(Podmiot)", ["Wartosc", "Rodzaj", "Eugenika", "Podmiot"], data[data["Nazwa"].str.startswith("B")]),
        ("C hip.1+3+5+7", "Wartosc ~ C(Rodzaj) * C(Eugenika) * C(Podmiot) * C(Powod)", ["Wartosc", "Rodzaj", "Eugenika", "Podmiot", "Powod"], data[data["Nazwa"].str.startswith("C")])
    ]

    ANOVA_table(prepared_data_historyjki, ".\Dane\ANOVA_tabelka_historyjki.txt")

    ANOVA_plot(data=data[data["Nazwa"].str.startswith("A")], x="Rodzaj", y="Wartosc", hue="Powod",
               title="Historia A - wykres średnich i odchyleń standardowych", xlab="Rodzaj pytania",
               ylab="Skala Likerta", fontsize=18, name_file="Wykresy\errorbar_ANOVA_std_hist_A.png")

    ANOVA_plot(data=data[data["Nazwa"].str.startswith("B")], x="Rodzaj", y="Wartosc", hue="Eugenika", col="Podmiot",
               title="Historia B - wykres średnich i odchyleń standardowych (podmiot)", xlab="Rodzaj pytania",
               ylab="Skala Likerta", fontsize=18, name_file="Wykresy\errorbar_ANOVA_std_hist_B_podmiot.png")

    ANOVA_plot(data=data[data["Nazwa"].str.startswith("B")], x="Rodzaj", y="Wartosc", hue="Eugenika", col="Doradca",
               title="Historia B - wykres średnich i odchyleń standardowych (doradca)", xlab="Rodzaj pytania",
               ylab="Skala Likerta", fontsize=18, name_file="Wykresy\errorbar_ANOVA_std_hist_B_doradca.png")

    ANOVA_plot(data=data[data["Nazwa"].str.startswith("B")], x="Rodzaj", y="Wartosc", hue="Eugenika", col="Zawod",
               title="Historia B - wykres średnich i odchyleń standardowych (zawod)", xlab="Rodzaj pytania",
               ylab="Skala Likerta", fontsize=18, name_file="Wykresy\errorbar_ANOVA_std_hist_B_zawod.png")

    ANOVA_plot(data=data[data["Nazwa"].str.startswith("C")], x="Rodzaj", y="Wartosc", hue="Eugenika", col="Podmiot", row="Powod",
               title="Historia C - wykres średnich i odchyleń standardowych", xlab="Rodzaj pytania",
               ylab="Skala Likerta", top_adj=0.91, name_file="Wykresy\errorbar_ANOVA_std_hist_C.png")
